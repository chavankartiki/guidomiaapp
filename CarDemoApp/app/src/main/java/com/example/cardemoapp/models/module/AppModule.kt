package com.example.cardemoapp.models.module

import com.example.cardemoapp.models.utils.Utils
import com.example.cardemoapp.models.ui.viewmodel.GuidomiaViewModel


import org.koin.dsl.module


var appModule = module {
    single { Utils() }
    factory { GuidomiaViewModel(get()) }
}
