package com.example.cardemoapp.models.db

import android.content.Context
import androidx.room.Room

object CarDBBuilder {

    private var carDatabase: CarDatabase? = null
    private val DB_NAME = "cars_db"

    /**
     * returns singleton instance of database
     */
    fun getInstance(context: Context): CarDatabase {
        if (carDatabase == null) {
            synchronized(CarDatabase::class) {
                carDatabase = buildRoomDB(context)
            }
        }
        return carDatabase!!
    }

    private fun buildRoomDB(context: Context) =
        Room.databaseBuilder(
            context.applicationContext,
            CarDatabase::class.java,
            DB_NAME
        ).build()
}