package com.example.cardemoapp.models.ui.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cardemoapp.databinding.ActivityMainBinding
import com.example.cardemoapp.models.adapter.CarListAdapter
import com.example.cardemoapp.models.model.Cars
import com.example.cardemoapp.models.model.FilterModel
import com.example.cardemoapp.models.ui.viewmodel.GuidomiaViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.java.KoinJavaComponent.inject

class GuidomiaActivity : AppCompatActivity() {

    private val viewModel: GuidomiaViewModel by inject(GuidomiaViewModel::class.java)
    private val recyclerViewAdapter = CarListAdapter(ArrayList())
    private var carList: ArrayList<Cars> = ArrayList()
    private var filterModel = FilterModel()
    private val activityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(activityMainBinding.root)

        initialize()

        // Filter Car data by Make
        activityMainBinding.editTextMake.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isNotEmpty()) {
                    filterModel.strCarMake = p0.toString()
                    viewModel.filterByMakeModel(filterModel)
                } else {
                    filterModel.strCarMake = ""
                    viewModel.filterByMakeModel(filterModel)
                }
                recyclerViewAdapter.notifyDataSetChanged()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })

        //Filter Car data by Model
        activityMainBinding.editTextModel.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString().isNotEmpty()) {
                    filterModel.strCarModel = p0.toString()
                    viewModel.filterByMakeModel(filterModel)
                } else {
                    filterModel.strCarModel = ""
                    viewModel.filterByMakeModel(filterModel)
                }
                recyclerViewAdapter.notifyDataSetChanged()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        })
    }

    /**
     * initialize recycler view and fetch the data
     */
    private fun initialize() {
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = recyclerViewAdapter

        observeFromViewModel()

        viewModel.fetchCarsFromDB(this)
    }

    /**
     * observe ViewModel Car Livedata
     */
    private fun observeFromViewModel() {
        viewModel.liveDataCars.observe(this) { cars ->
            recyclerViewAdapter.loadData(cars)
            carList = cars
        }
    }
}