package com.example.cardemoapp.models.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.cardemoapp.models.model.Cars

@Database(
    entities = [Cars::class],
    version = 1
)
@TypeConverters(CarDBConverter::class)
abstract class CarDatabase : RoomDatabase() {
    abstract fun getCarDao(): CarDao
}