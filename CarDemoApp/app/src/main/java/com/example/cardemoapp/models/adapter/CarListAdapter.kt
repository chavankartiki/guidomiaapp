package com.example.cardemoapp.models.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.cardemoapp.R
import com.example.cardemoapp.databinding.ItemCarListBinding
import com.example.cardemoapp.models.model.Cars
import com.example.cardemoapp.models.utils.ALPINE
import com.example.cardemoapp.models.utils.BMW
import com.example.cardemoapp.models.utils.LAND_ROVER
import com.example.cardemoapp.models.utils.MERCEDES_BENZ

private var previousExpandedPosition = 0
private var mExpandedPosition = 0

class CarListAdapter(private var cars: ArrayList<Cars>) :
    RecyclerView.Adapter<CarListAdapter.CarViewHolder>() {

    /**
     * ViewHolder class for Car data views binding
     * @param binding
     */
    inner class CarViewHolder(private val itemCarListBinding: ItemCarListBinding) :
        RecyclerView.ViewHolder(itemCarListBinding.root) {
        fun bind(position: Int) {
            val car = cars[position]

            val linearLayoutProsCons =
                LayoutInflater.from(itemView.context).context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            when (car.make) {
                LAND_ROVER -> itemCarListBinding.imageViewCar.setImageResource(R.drawable.range_rover)
                ALPINE -> itemCarListBinding.imageViewCar.setImageResource(R.drawable.alpine_roadster)
                BMW -> itemCarListBinding.imageViewCar.setImageResource(R.drawable.bmw_330i)
                MERCEDES_BENZ -> itemCarListBinding.imageViewCar.setImageResource(R.drawable.mercedez_benz_glc)
            }
            itemCarListBinding.textViewCarName.text = "${car.make} ${car.model}"
            val convPrice: Int = (car.marketPrice / 1000)
            "${convPrice}k".also { itemCarListBinding.textViewCarPrice.text = it }

            itemCarListBinding.ratingBar.rating = car.rating.toFloat()
            if (car.prosList?.isEmpty() == true) {
                itemCarListBinding.llPros.visibility = View.GONE
            } else {
                itemCarListBinding.llPros.removeAllViews()
                car.prosList?.forEach {
                    if (it.isNotEmpty()) {
                        val view: View = linearLayoutProsCons.inflate(
                            R.layout.item_pros_cons,
                            itemCarListBinding.llPros,
                            false
                        )
                        val title: TextView = view.findViewById(R.id.tvProsOrCons)
                        title.text = it
                        itemCarListBinding.llPros.addView(view)
                    }
                }
            }

            if (car.consList?.isEmpty() == true) {
                itemCarListBinding.llCons.visibility = View.GONE
            } else {
                itemCarListBinding.llCons.removeAllViews()
                car.consList?.forEach {
                    if (it.isNotEmpty()) {
                        val view: View = linearLayoutProsCons.inflate(
                            R.layout.item_pros_cons,
                            itemCarListBinding.llCons,
                            false
                        )
                        val title: TextView = view.findViewById(R.id.tvProsOrCons)
                        title.text = it
                        itemCarListBinding.llCons.addView(view)
                    }
                }
            }

            val isExpandable: Boolean = (position == mExpandedPosition)
            itemCarListBinding.expandableLayout.visibility =
                if (isExpandable) View.VISIBLE else View.GONE
            itemCarListBinding.constraintLayout.isActivated = isExpandable

            if (isExpandable) previousExpandedPosition = position

            itemCarListBinding.constraintLayout.setOnClickListener {
                mExpandedPosition = if (isExpandable) -1 else position
                notifyItemChanged(previousExpandedPosition)
                notifyItemChanged(position)
            }
        }
    }

    /**
     * Load Data to ArrayList and Notify
     */
    fun loadData(newCars: ArrayList<Cars>) {
        cars = newCars
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder {
        val itemCarListBinding =
            ItemCarListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CarViewHolder(itemCarListBinding)
    }

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return cars.size
    }
}