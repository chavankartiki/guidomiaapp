package com.example.cardemoapp.models.ui.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cardemoapp.models.db.CarDBBuilder
import com.example.cardemoapp.models.db.CarDao
import com.example.cardemoapp.models.model.Cars
import com.example.cardemoapp.models.model.FilterModel
import com.example.cardemoapp.models.utils.JSON_FILE_NAME
import com.example.cardemoapp.models.utils.Utils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GuidomiaViewModel(private val utils: Utils) : ViewModel() {

    private val mutableLiveDataCars = MutableLiveData<ArrayList<Cars>>()
    val liveDataCars: LiveData<ArrayList<Cars>> get() = mutableLiveDataCars
    private var filteredCarList = ArrayList<Cars>()

    /**
     * Fetch car list data either from DB or JSON
     * @param context
     */
    fun fetchCarsFromDB(context: Context) {
        viewModelScope.launch {
            val carResult = withContext(Dispatchers.IO) {
                (getCarDBObject(context).getCarList() as ArrayList<Cars>)
            }
            if (carResult.isEmpty()) {
                fetchCarsFromJSON(context)
            } else {
                mutableLiveDataCars.value = carResult
                filteredCarList = carResult
            }
        }
    }

    /**
     * Fetch car lis data from JSON
     * @param context
     */
    private fun fetchCarsFromJSON(context: Context) {
        viewModelScope.launch {
            val jsonString =
                withContext(Dispatchers.IO) {
                    utils.getCarJSONDataFromAssets(
                        context,
                        JSON_FILE_NAME
                    )
                }

            val gson = Gson()
            val listCarType = object : TypeToken<ArrayList<Cars>>() {}.type

            val cars = gson.fromJson<ArrayList<Cars>>(jsonString, listCarType)
            filteredCarList = cars
            mutableLiveDataCars.value = cars
            insertCarList(cars, context)
        }

    }

    private fun getCarDBObject(context: Context): CarDao {
        return CarDBBuilder.getInstance(context).getCarDao()
    }

    /**
     * Insert Car List data in Room database
     * @param carList,context
     */
    private fun insertCarList(carList: ArrayList<Cars>, context: Context) {
        viewModelScope.launch {
            launch { getCarDBObject(context).insertCarList(carList) }
        }
    }

    /**
     * Filter Car data by make
     * @param carMake
     */
    private fun filtersCarByMake(carMake: String) {
        val arrayListLocalFilter = ArrayList<Cars>()
        arrayListLocalFilter.clear()

        filteredCarList.forEach {
            if (it.make.contains(carMake, true))
                arrayListLocalFilter.add(it)
        }
        mutableLiveDataCars.value = arrayListLocalFilter
    }

    /**
     * Filter Car data by Model
     * @param carModel
     */
    private fun filtersCarByModel(carModel: String) {
        val arrayListLocalFilter = ArrayList<Cars>()
        arrayListLocalFilter.clear()

        filteredCarList.forEach {
            if (it.model.contains(carModel, true))
                arrayListLocalFilter.add(it)
        }
        mutableLiveDataCars.value = arrayListLocalFilter
    }

    /**
     * Filter Car Data by Make and Model
     * @param carMake and model
     */
    private fun filtersCarByMakeModel(carMake: String, carModel: String) {
        val arrayListLocalFilter = ArrayList<Cars>()
        arrayListLocalFilter.clear()

        filteredCarList.forEach {
            if (it.model.contains(carModel, true) && it.make.contains(carMake, true))
                arrayListLocalFilter.add(it)
        }
        mutableLiveDataCars.value = arrayListLocalFilter
    }

    /**
     * Filter by make and model depending upon selection
     * @param filterModel
     */
    fun filterByMakeModel(filterModel: FilterModel) {
        if (!filterModel.strCarMake.isNullOrEmpty() && !filterModel.strCarModel.isNullOrEmpty()) {
            filtersCarByMakeModel(filterModel.strCarMake!!, filterModel.strCarModel!!)
        } else if (!filterModel.strCarMake.isNullOrEmpty()) {
            filterModel.strCarMake?.let { filtersCarByMake(it) }
        } else if (!filterModel.strCarModel.isNullOrEmpty()) {
            filterModel.strCarModel?.let { filtersCarByModel(it) }
        } else {
            mutableLiveDataCars.value = filteredCarList
        }
    }
}