package com.example.cardemoapp.models.utils

const val LAND_ROVER = "Land Rover"
const val ALPINE = "Alpine"
const val BMW = "BMW"
const val MERCEDES_BENZ = "Mercedes Benz"
const val JSON_FILE_NAME = "cars.json"