package com.example.cardemoapp.models.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cars")
data class Cars(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    val consList: ArrayList<String>? = null,
    val customerPrice: Int = 0,
    val make: String = "",
    val marketPrice: Int = 0,
    val model: String = "",
    val prosList: ArrayList<String>? = null,
    val rating: Int = 0,
    var expandable: Boolean = false

)