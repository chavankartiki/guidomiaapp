package com.example.cardemoapp.models

import android.app.Application


import com.example.cardemoapp.models.module.appModule

import org.koin.core.context.startKoin


class GuidomiaApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(appModule)
        }

    }
}