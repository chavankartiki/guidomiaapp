package com.example.cardemoapp.models.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.cardemoapp.models.model.Cars

@Dao
interface CarDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCarList(carList: ArrayList<Cars>)

    @Query("SELECT * FROM cars")
    fun getCarList(): List<Cars>
}