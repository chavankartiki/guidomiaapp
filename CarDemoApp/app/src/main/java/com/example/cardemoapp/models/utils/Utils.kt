package com.example.cardemoapp.models.utils

import android.content.Context
import java.io.IOException

class Utils {

    /**
     * Get Data from JSON
     * @param context
     */
    fun getCarJSONDataFromAssets(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            println(ioException.message)
            return null
        }
        return jsonString
    }
}